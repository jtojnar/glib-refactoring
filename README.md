# glib-refactoring

This is a set of semantic patches for [Coccinelle](http://coccinelle.lip6.fr) for various code transformations of GLib-based and GNOME project.

After installing Coccinelle, you can preview what individual patches would do like this:

```
spatch --sp-file patches/gtk3/asynchronize-dialog-without-result.cocci --dir ~/Projects/file-roller/src/
```

The command will output a unified patch of the transformation it would carry out. If you are happy with it, you can apply it by running the command again with `--in-place` argument.

## Available patches

Please always check the individual patch files to see their limitations.


### All GLib

- [`glib/autocleanup.cocci`] replaces manual resource management by automatic clean-up macros.
- [`glib/clear-object.cocci`] replaces conditional `g_object_unref` with `g_clear_object`.
- [`do-not-cast-signal-connect-object.cocci`] removes unnecessary `G_OBJECT` casts in `g_signal_connect`.
- [`find-non-void-signals.cocci`] finds and prints signals with a non-void return value.
- [`voidize-signal-handlers.cocci`] changes the return value of signal handlers to void since that is what most signal handlers should return.


### GTK 2

- [`gtk2/direct-property-access-to-functions.cocci`] replaces direct property access on GTK objects by getters and setters.
- [`gtk2/fixup-outparam-functions.cocci`] fixes some incorrect replacements by `gtk2/direct-property-access-to-functions.cocci`.
- [`gtk2/keysym-compat.cocci`] replaces legacy keysym constants.


### GTK 3

- [`gtk3/asynchronize-dialog-without-result.cocci`] replaces `gtk_dialog_run` by async for dialogs whose response we do not care about.
- [`gtk3/cursor-deprecations.cocci`] replaces legacy X server cursors with named ones.


## Related work

- [tartan](https://gitlab.freedesktop.org/tartan/tartan) – analysis
- [gtk-rewriter](https://web.archive.org/web/20080823051200/http://people.imendio.com/richard/gtk-rewriter/) – source code lost?


## Interesting resources

- [Coccinelle documentation](https://coccinelle.gitlabpages.inria.fr/website/documentation.html)
- Especially the [Coccinelle tutorial](https://www.lrz.de/services/compute/courses/x_lecturenotes/hspc1w19.pdf) from Leibniz Supercomputing Centre but also other Coccinelle [Papers and Slides](https://coccinelle.gitlabpages.inria.fr/website/papers.html)
- [SmPL grammar](https://coccinelle.gitlabpages.inria.fr/website/docs/index.html)


## License

The code of this project is licensed under the terms of the MIT license.

[`glib/autocleanup.cocci`]: patches/glib/autocleanup.cocci
[`glib/clear-object.cocci`]: patches/glib/clear-object.cocci
[`gtk3/asynchronize-dialog-without-result.cocci`]: patches/gtk3/asynchronize-dialog-without-result.cocci
[`gtk3/cursor-deprecations.cocci`]: patches/gtk3/cursor-deprecations.cocci
[`do-not-cast-signal-connect-object.cocci`]: patches/do-not-cast-signal-connect-object.cocci
[`find-non-void-signals.cocci`]: patches/find-non-void-signals.cocci
[`gtk2/direct-property-access-to-functions.cocci`]: patches/gtk2/direct-property-access-to-functions.cocci
[`gtk2/fixup-outparam-functions.cocci`]: patches/gtk2/fixup-outparam-functions.cocci
[`gtk2/keysym-compat.cocci`]: patches/gtk2/keysym-compat.cocci
[`voidize-signal-handlers.cocci`]: patches/voidize-signal-handlers.cocci
