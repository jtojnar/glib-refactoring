{
  description = "Tool for refactoring GLib-based projects";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , flake-compat
    , nixpkgs
    , utils
    }:

    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShells = {
          default = pkgs.mkShell {
            nativeBuildInputs = [
              pkgs.clang-tools_14
              pkgs.coccinelle
              pkgs.python3
              # Required by pyml to find Python.
              pkgs.which
            ];
          };
        };
      }
  );
}
