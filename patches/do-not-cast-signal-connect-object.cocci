/**
 * Removes spurious G_OBJECT casts in g_signal_connect macros.
 *
 * They are not necessary since macros are not type checked.
 * Even worse, they actually mask the object type for Tartan
 * preventing it from validating the handler type.
 *
 * You should be able to run this and then follow Tartan instructions
 * to manually add more specific type casts where necessary
 * (e.g. when the variable holding the object uses parent type).
 */

@@
expression object;
@@
(g_signal_connect
|g_signal_connect_after
|g_signal_connect_swapped
)(
- G_OBJECT (object)
+ object
, ...)
