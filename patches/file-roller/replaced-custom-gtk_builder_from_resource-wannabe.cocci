/**
 * Replaces custom _gtk_builder_new_from_resource
 * by gtk_builder_new_from_resource, which is identical
 * except that it fails loudly.
 * Given that resources are baked into the app,
 * not being able to load ui file should be a hard error anyway.
 */

@initialize:python@
@@


@replace_custom@
constant path;
// Ugly hack since cocci syntax does not support constant concatenation.
// https://github.com/coccinelle/coccinelle/issues/276
fresh identifier resource_path = script:python (path) { "FILE_ROLLER_RESOURCE_UI_PATH " + path };
@@
-_gtk_builder_new_from_resource(path)
+gtk_builder_new_from_resource(resource_path)


@drop_null_check depends on replace_custom@
expression variable;
@@
variable = gtk_builder_new_from_resource (...);
...
-if (variable == NULL) { ... }


@drop_function@
@@
-_gtk_builder_new_from_resource(...) { ... }
