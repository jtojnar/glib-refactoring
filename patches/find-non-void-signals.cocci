/**
 * Finds and prints signals with a non-void return value.
 */

@initialize:python@
@@
import json

def get_containing_function(pos):
    return pos[0].current_element

def is_false_positive(pos):
    """
    Ignore false positives caused by the constructors being defined in terms of each other.
    """
    return get_containing_function(pos) in ["g_signal_new", "g_signal_new_class_handler", "g_signal_new_valist"]

def does_fun_correspond(pos, class_init_function):
    """
    Check if the function the position is from corresponds to the currently matched function.
    """
    return class_init_function == get_containing_function(pos)


@find@
identifier class_var;
expression signal_name, itype, signal_flags, class_something, accumulator, accu_data, c_marshaller;
// Filter out void return values.
expression return_type != {G_TYPE_NONE};
// We use position to find out function name for successive matching
// and also to filter out GSignal constructors themselves.
position pos: script:python() { not is_false_positive(pos) };
@@
\( g_signal_new \| g_signal_new_class_handler \| g_signal_new_valist \| g_signal_newv \)(\( I_(signal_name) \| signal_name \), \( G_OBJECT_CLASS_TYPE(class_var) \| G_TYPE_FROM_CLASS(class_var) \| itype \), signal_flags, class_something, accumulator, accu_data, c_marshaller, return_type, ...)@pos


@extract_params depends on find@
identifier class_init_function : script:python(find.pos) { does_fun_correspond(pos, class_init_function) };
type class_type;
identifier find.class_var;
// The class variable is sometimes recast.
identifier class_var_not_matching;
// Not even sure if the class_init functions can take any extra parameters,
// let’s log them just to be safe.
parameter list extra_params;
@@
class_init_function(class_type *\( class_var \| class_var_not_matching \), extra_params) { ... }


@script:python depends on extract_params@
signal_name << find.signal_name;
class_type << extract_params.class_type;
itype << find.itype = "";
extra_params << extract_params.extra_params;
return_type << find.return_type;
@@
data = {"class_type": class_type, "signal_name": signal_name, "return_type": return_type}
extra_params = list(extra_params)
if extra_params:
    data["extra_params"] = extra_params
if itype:
    data["itype"] = itype

print(json.dumps(data))
