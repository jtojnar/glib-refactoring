/**
 * Replaces manual resource management by automatic clean-up macros.
 *
 * Issues:
 *  - Not all types supporting auto-cleanup are replaced.
 */

@initialize:python@
@@

autofree_functions = {
    "g_free",
}

autoptr_functions = {
    "g_closure_unref",
    "g_type_class_unref",
    "g_object_unref",
    "g_param_spec_unref",
    "g_unix_mount_free",
    "g_unix_mount_point_free",
    "g_async_queue_unref",
    "g_bookmark_file_free",
    "g_bytes_unref",
    "g_checksum_free",
    "g_date_time_unref",
    "g_date_free",
    "g_dir_close",
    "g_error_free",
    "g_hash_table_unref",
    "g_hmac_unref",
    "g_io_channel_unref",
    "g_key_file_free",
    "g_key_file_unref",
    "g_list_free",
    "g_array_unref",
    "g_ptr_array_unref",
    "g_byte_array_unref",
    "g_main_context_unref",
    "g_main_context_pusher_free",
    "g_main_loop_unref",
    "g_source_unref",
    "g_mapped_file_unref",
    "g_markup_parse_context_unref",
    "g_node_destroy",
    "g_option_context_free",
    "g_option_group_unref",
    "g_pattern_spec_free",
    "g_queue_free",
    "g_rand_free",
    "g_regex_unref",
    "g_match_info_unref",
    "g_scanner_destroy",
    "g_sequence_free",
    "g_slist_free",
    "g_autoptr_cleanup_gstring_free",
    "g_string_chunk_free",
    "g_strv_builder_unref",
    "g_thread_unref",
    "g_mutex_locker_free",
    "g_rec_mutex_locker_free",
    "g_rw_lock_writer_locker_free",
    "g_rw_lock_reader_locker_free",
    "g_timer_destroy",
    "g_time_zone_unref",
    "g_tree_unref",
    "g_variant_unref",
    "g_variant_builder_unref",
    "g_variant_iter_free",
    "g_variant_dict_unref",
    "g_variant_type_free",
    "g_ref_string_release",
    "g_uri_unref",
    "g_dbus_node_info_unref",
    "g_file_attribute_info_list_unref",
    "g_resource_unref",
    "g_settings_schema_unref",
    "g_settings_schema_key_unref",
    "g_settings_schema_source_unref",
}

autocleanup_functions = autofree_functions | autoptr_functions

def make_autocleanup_type(type_name, var, destroy):
    type_name = type_name.strip()
    if destroy in autofree_functions:
        prefix = f"g_autofree {type_name}"
    else:
        assert type_name.endswith("*"), f"Type “{type_name}” is cleaned with “{destroy}” function but is not a pointer."
        # autoptr takes type name, not a pointer type.
        type_name = type_name[:-1].strip()
        prefix = f"g_autoptr({type_name})"

    return f"{prefix} {var.strip()}"

def make_autoptr_type(type_name, var):
    return f"g_autoptr({type_name.strip()}) {var.strip()}"


@pre_match_decls disable decl_init exists@
identifier var;
type T;
expression init;
position pos;
identifier destroy : script:python() { (destroy in autocleanup_functions) };
@@

(
    T var@pos;
|
    T var@pos = init;
)
...
destroy(var);


@replace_with_autocleanup disable decl_init@
identifier pre_match_decls.var;
type pre_match_decls.T;
expression pre_match_decls.init;
position pre_match_decls.pos;
identifier pre_match_decls.destroy;
fresh identifier autofree_T_var = script:python(T, var, destroy) { make_autocleanup_type(T, var, destroy) };
@@

(
-    T var@pos;
+    autofree_T_var = NULL;
|
-    T var@pos = init;
+    autofree_T_var = init;
)
<...
-    destroy(var);
...>
?\(T var;\|T var = ...;\) // if this declaration reappears (e.g. in a next loop iteration), stop matching
