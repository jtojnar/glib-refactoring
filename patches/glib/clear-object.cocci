/**
 * Replaces conditional g_object_unref with g_clear_object.
 */

@@
expression obj;
@@

-if (obj != NULL) {
-    g_object_unref(obj);
-    obj = NULL;
-}
+g_clear_object(&obj);
