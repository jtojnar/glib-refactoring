/**
 * Replaces direct access to struct members on GTK objects with getters.
 *
 * Issues:
 *  - This is mostly just deals with the simple cases where an accessor is used on a variable,
 *    not on any more complex expressions so e.g. nested accessors will not work.
 *  - It does not handle methods without parameters so you might want to run
 *    `gtk2/fixup-outparam-functions.cocci` after this.
 *  - It only handles a few unsealed structs (e.g. GtkAllocation).
 *
 * https://docs.gtk.org/gtk3/migrating-2to3.html#use-accessor-functions-instead-of-direct-access
 */

@initialize:python@
@@

import re

unsealed_structs = [
    "gtk_allocation",
    "gtk_requisition",
]

def is_gtk_object(type_name):
    type_name = type_name.strip()
    match = re.match(r"^Gtk\S+(?<!Class)\s+\*$", type_name)
    return match is not None and make_prefix_from_type(type_name) not in unsealed_structs

def is_gtk_object_cast(identifier):
    identifier = identifier.strip()
    match = re.match(r"^GTK_.+(?<!_CLASS)$", identifier)
    return match is not None and identifier.lower() not in unsealed_structs

def camel_to_snake_case(string):
    return re.sub(r"(?<!^)(?=[A-Z])", "_", string).lower()

def make_prefix_from_type(gtk_type):
    return camel_to_snake_case(gtk_type.strip().strip("*").strip())

def make_prefix_from_cast(gtk_type_cast):
    return gtk_type_cast.strip().lower()

def make_method_name(prefix, member_name, is_setter):
    letter = "s" if is_setter else "g"
    return f"{prefix}_{letter}et_{member_name}"

def make_setter_name(gtk_type, member_name):
    return make_method_name(gtk_type, member_name, True)

def make_getter_name(gtk_type, member_name):
    return make_method_name(gtk_type, member_name, False)


@@
// Setter on function parameters
type gtk_type : script:python() { is_gtk_object(gtk_type) };
identifier fn;
identifier object_var;
identifier member_name;
expression value;
fresh identifier setter_name = script:python(gtk_type, member_name) { make_setter_name(make_prefix_from_type(gtk_type), member_name) };
@@

fn(..., gtk_type object_var, ...) {
    <+...
-    object_var->member_name = value;
+    setter_name(object_var, value);
    ...+>
}


@@
// Getter on function parameters
type gtk_type : script:python() { is_gtk_object(gtk_type) };
identifier fn;
identifier object_var;
identifier member_name;
fresh identifier getter_name = script:python(gtk_type, member_name) { make_getter_name(make_prefix_from_type(gtk_type), member_name) };
@@

fn(..., gtk_type object_var, ...) {
    <+...
-    object_var->member_name
+    getter_name(object_var)
    ...+>
}


@@
// Setter on declared variables
type gtk_type : script:python() { is_gtk_object(gtk_type) };
identifier object_var;
identifier member_name;
expression value;
fresh identifier setter_name = script:python(gtk_type, member_name) { make_setter_name(make_prefix_from_type(gtk_type), member_name) };
@@

gtk_type object_var;
<+...
-object_var->member_name = value;
+setter_name(object_var, value);
...+>


@@
// Getter on declared variables
type gtk_type : script:python() { is_gtk_object(gtk_type) };
identifier object_var;
identifier member_name;
fresh identifier getter_name = script:python(gtk_type, member_name) { make_getter_name(make_prefix_from_type(gtk_type), member_name) };
@@

gtk_type object_var;
<+...
-object_var->member_name
+getter_name(object_var)
...+>


@@
// Setter on cast expressions
identifier gtk_type_cast : script:python() { is_gtk_object_cast(gtk_type_cast) };
expression object_expr;
identifier member_name;
expression value;
fresh identifier setter_name = script:python(gtk_type_cast, member_name) { make_setter_name(make_prefix_from_cast(gtk_type_cast), member_name) };
@@

-(gtk_type_cast(object_expr))->member_name = value;
+setter_name(gtk_type_cast(object_expr), value);


@@
// Getter on cast expressions
identifier gtk_type_cast : script:python() { is_gtk_object_cast(gtk_type_cast) };
expression object_expr;
identifier member_name;
fresh identifier getter_name = script:python(gtk_type_cast, member_name) { make_getter_name(make_prefix_from_cast(gtk_type_cast), member_name) };
@@

-(gtk_type_cast(object_expr))->member_name
+getter_name(gtk_type_cast(object_expr))
