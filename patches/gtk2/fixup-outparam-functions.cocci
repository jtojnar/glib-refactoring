/**
 * Fixes replacements that could not have been done by `gtk2/fixup-outparam-functions.cocci`.
 *
 * Issues:
 *  - The list of supported functions is limited to what we encountered in the wild so far.
 */

@@
// Match the statement/declaration containing the `GtkAllocation` use.
expression widget;
statement S;
declaration D;
fresh identifier allocation = "allocation";
@@

(
    \(
++        GtkAllocation allocation;
++        gtk_widget_get_allocation(widget, &allocation);
        S
    \|
++        GtkAllocation allocation;
++        gtk_widget_get_allocation(widget, &allocation);
        D
    \)
&
-    gtk_widget_get_allocation(widget)
+    allocation
)


@@
// Fix setter receiving GtkAllocation instead of a pointer.
expression widget;
expression allocation_ptr;
@@

-gtk_widget_set_allocation(widget, *allocation_ptr);
+gtk_widget_set_allocation(widget, allocation_ptr);


@@
expression cell;
expression xalign;
expression yalign;
@@

-gtk_cell_renderer_set_xalign(cell, xalign);
-gtk_cell_renderer_set_yalign(cell, yalign);
+gtk_cell_renderer_set_alignment(cell, xalign, yalign);


@@
expression cell;
statement S;
declaration D;
fresh identifier xpad = "xpad";
fresh identifier ypad = "ypad";
@@

(
    \(
++        gint xpad;
++        gint ypad;
++        gtk_cell_renderer_get_padding(cell, &xpad, &ypad);
        S
    \|
++        gint xpad;
++        gint ypad;
++        gtk_cell_renderer_get_padding(cell, &xpad, &ypad);
        D
    \)
&
    \(
-    gtk_cell_renderer_get_xpad(cell)
+    xpad
    \|
-    gtk_cell_renderer_get_ypad(cell)
+    ypad
    \)
)


@@
expression cell;
statement S;
declaration D;
fresh identifier height = "height";
fresh identifier width = "width";
// Docs refer to these as fixed width and fixed size
// https://developer-old.gnome.org/gtk2/stable/GtkCellRenderer.html#GtkCellRenderer--height
@@

(
    \(
++        gint width;
++        gint height;
++        gtk_cell_renderer_get_fixed_size(cell, &width, &height);
        S
    \|
++        gint width;
++        gint height;
++        gtk_cell_renderer_get_fixed_size(cell, &width, &height);
        D
    \)
&
    \(
-    gtk_cell_renderer_get_width(cell)
+    width
    \|
-    gtk_cell_renderer_get_height(cell)
+    height
    \)
)


@@
expression cell_renderer;
expression xpad;
expression ypad;
@@

-gtk_cell_renderer_set_xpad(cell_renderer, xpad);
-gtk_cell_renderer_set_ypad(cell_renderer, ypad);
+gtk_cell_renderer_set_padding(cell_renderer, xpad, ypad);


@@
expression cell_renderer;
expression mode;
@@

-gtk_cell_renderer_set_mode(cell_renderer, mode);
+g_object_set(cell_renderer, "mode", mode, NULL);
