/**
 * Replaces `gtk_dialog_run` by async for dialogs whose response we do not care about.
 *
 * Issues:
 *  - Only handles gtk_dialog_run whose whose return value is not stored into a variable.
 *    To do it properly would require control flow graph decomposition.
 *  - By moving code to callback local data is inaccessible. TODO: change local variable accesses to user_data->var.
 *  - It breaks programs expecting that the function only returns after dismissing the dialogue,
 *    e.g. for confirming that the user is ready.
 *  - Moving the code after run into a separate function strips comments.
 *  - The callbacks end up defined in mostly reverse order.
 */

@initialize:python@
@@
counters = {}

def get_index(name):
    if name not in counters:
        counters[name] = 0
    counters[name] += 1
    return counters[name]

def make_fresh(name):
    return f"{name}{get_index(name)}"

def get_containing_function(pos):
    return pos[0].current_element


@replace_run@
// We use position to find out function name for successive matching.
position pos;
expression dialog_widget;
// This does not actually work since all the matches share a single instances of the rule.
fresh identifier response_cb = script:python(pos) { make_fresh(get_containing_function(pos) + "_response_cb") };
fresh identifier begin_callback = "begin_callback";
@@
(
// Variant 1: No code after running other than destroy.
// We can just connect it to the signal directly.
-gtk_dialog_run (GTK_DIALOG (dialog_widget));@pos
-gtk_widget_destroy (\(GTK_WIDGET (dialog_widget) \| dialog_widget\));
+g_signal_connect (dialog_widget, "response", G_CALLBACK (gtk_widget_destroy), NULL);
+gtk_widget_show (dialog_widget);
return ...;
|
// Variant 2: There is more code.
// We need to move it out into a callback.
// Since Coccinelle does not allow matching list of statements outside of block,
// we need to wrap it first.
-gtk_dialog_run (GTK_DIALOG (dialog_widget));@pos
+g_signal_connect (dialog_widget, "response", G_CALLBACK (response_cb), NULL);
+gtk_widget_show (dialog_widget);
+begin_callback (response_cb);
+{
...
gtk_widget_destroy (
-    \(GTK_WIDGET (dialog_widget) \| dialog_widget\)
+    GTK_WIDGET (dialog)
 );
...
+}
return ...;
)


// Then we can actually match the callback content.
@extract_callback depends on replace_run@
identifier replace_run.begin_callback;
identifier response_cb;
statement list sl;
@@
-begin_callback (response_cb);
-{
-sl
-}


// To finally create a new function with the extracted body.
@add_response_cb depends on extract_callback@
identifier func : script:python(replace_run.pos) { func == get_containing_function(pos) };
identifier extract_callback.response_cb;
statement list extract_callback.sl;
typedef gconstpointer;
typedef GtkDialog;
@@
++static void response_cb(GtkDialog *dialog, int response, gconstpointer data) {
++    sl
++}
// And prepend it before the original function.
func(...) { ... }
