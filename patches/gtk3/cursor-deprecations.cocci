/**
 * Replaces `gdk_cursor_new` and `gdk_cursor_new_for_display` by `gdk_cursor_new_from_name`.
 *
 * Issues:
 *  - If there is no straightforward mapping from `GdkCursorType` to a cursor name, the constant will be left as is. You may want to compare
 *   the [legacy cursors](https://docs.gtk.org/gdk3/enum.CursorType.html) with the [new ones](https://docs.gtk.org/gdk3/ctor.Cursor.new_from_name.html) and choose for yourself.
 */

@initialize:python@
@@
"""
A dictionary translating from GdkCursorType to CSS names.
Mostly visually matched with the help of the following sites:
 - https://docs.gtk.org/gdk3/enum.CursorType.html
 - https://docs.gtk.org/gdk3/ctor.Cursor.new_from_name.html
 - https://tronche.com/gui/x/xlib/appendix/b/
 - https://developer.mozilla.org/en-US/docs/Web/CSS/cursor
"""
cursor_translation_map = {
    "GDK_X_CURSOR": "not-allowed",
    "GDK_ARROW": "default",
    "GDK_BASED_ARROW_DOWN": "s-resize",
    "GDK_BASED_ARROW_UP": "n-resize",
    "GDK_BOAT": None,
    "GDK_BOGOSITY": None,
    "GDK_BOTTOM_LEFT_CORNER": "sw-resize",
    "GDK_BOTTOM_RIGHT_CORNER": "se-resize",
    "GDK_BOTTOM_SIDE": "s-resize",
    "GDK_BOTTOM_TEE": None,
    "GDK_BOX_SPIRAL": None,
    "GDK_CENTER_PTR": "default",
    "GDK_CIRCLE": None,
    "GDK_CLOCK": None,
    "GDK_COFFEE_MUG": None,
    "GDK_CROSS": "crosshair",
    "GDK_CROSS_REVERSE": "crosshair",
    "GDK_CROSSHAIR": "crosshair",
    "GDK_DIAMOND_CROSS": None,
    "GDK_DOT": None,
    "GDK_DOTBOX": None,
    "GDK_DOUBLE_ARROW": "ns-resize",
    "GDK_DRAFT_LARGE": "default",
    "GDK_DRAFT_SMALL": "default",
    "GDK_DRAPED_BOX": None,
    "GDK_EXCHANGE": None,
    "GDK_FLEUR": "move",
    "GDK_GOBBLER": None,
    "GDK_GUMBY": None,
    "GDK_HAND1": "grab",
    "GDK_HAND2": "pointer",
    "GDK_HEART": None,
    "GDK_ICON": None,
    "GDK_IRON_CROSS": None,
    "GDK_LEFT_PTR": "default",
    "GDK_LEFT_SIDE": "w-resize",
    "GDK_LEFT_TEE": None,
    "GDK_LEFTBUTTON": None,
    "GDK_LL_ANGLE": None,
    "GDK_LR_ANGLE": None,
    "GDK_MAN": None,
    "GDK_MIDDLEBUTTON": None,
    "GDK_MOUSE": None,
    "GDK_PENCIL": None,
    "GDK_PIRATE": None,
    "GDK_PLUS": "cell",
    "GDK_QUESTION_ARROW": "help",
    "GDK_RIGHT_PTR": None,
    "GDK_RIGHT_SIDE": "e-resize",
    "GDK_RIGHT_TEE": None,
    "GDK_RIGHTBUTTON": None,
    "GDK_RTL_LOGO": None,
    "GDK_SAILBOAT": None,
    "GDK_SB_DOWN_ARROW": None,
    "GDK_SB_H_DOUBLE_ARROW": "ew-resize",
    "GDK_SB_LEFT_ARROW": None,
    "GDK_SB_RIGHT_ARROW": None,
    "GDK_SB_UP_ARROW": None,
    "GDK_SB_V_DOUBLE_ARROW": "ns-resize",
    "GDK_SHUTTLE": None,
    "GDK_SIZING": None,
    "GDK_SPIDER": None,
    "GDK_SPRAYCAN": None,
    "GDK_STAR": None,
    "GDK_TARGET": None,
    "GDK_TCROSS": None,
    "GDK_TOP_LEFT_ARROW": None,
    "GDK_TOP_LEFT_CORNER": "nw-resize",
    "GDK_TOP_RIGHT_CORNER": "ne-resize",
    "GDK_TOP_SIDE": "n-resize",
    "GDK_TOP_TEE": None,
    "GDK_TREK": None,
    "GDK_UL_ANGLE": None,
    "GDK_UMBRELLA": None,
    "GDK_UR_ANGLE": None,
    "GDK_WATCH": "wait",
    "GDK_XTERM": "text",
    "GDK_LAST_CURSOR": None,
    "GDK_BLANK_CURSOR": "none",
}

def cursor_name_from_type(cursor_type):
    name = cursor_translation_map.get(cursor_type.strip(), None)
    if name is not None:
        return f'"{name}"'
    else:
        # Keep the cursor as is if the cursor type does not have an equivalent.
        return cursor_type


@gdk_cursor_new@
// gdk_cursor_new is deprecated since GTK 3.16
// https://docs.gtk.org/gdk3/ctor.Cursor.new.html
expression cursor_type;
@@
-gdk_cursor_new(cursor_type)
+gdk_cursor_new_for_display(gdk_display_get_default(), cursor_type)


@remove_cursor_type@
// GTK 4 will remove GdkCursorType
// https://docs.gtk.org/gdk3/enum.CursorType.html
expression display;
constant cursor_type;
fresh identifier cursor_name = script:python(cursor_type) { cursor_name_from_type(cursor_type) };
@@
-gdk_cursor_new_for_display(display, cursor_type)
+gdk_cursor_new_from_name(display, cursor_name)
