/**
 * Changes the return value of ALL signal handlers to void since that is what most signal handlers should return.
 * It also removes the return values.
 *
 * ⚠ This patch is extra destructive so make sure you have code staged in version control before applying it.
 *
 * Issues:
 *  - It does not know the type of the signal handlers so it might transform even handlers that should return value.
 *   Please run `find-non-void-signals.cocci` on GTK source code, or analyze your code with Tartan to see which
 *   signal handlers are incorrectly typed.
 */

@initialize:python@
@@
# List of signals which return a value so we should ignore their callback.
# Currently, we are not able to distinguish the object class so we need to be wary
# of multiple signals with the same name but different return values.
# Hopefully, this kind of collision will be rare.
signals_returning_values = {
    '"button-press-event"',
    '"button-release-event"',
    '"drag-motion"',
    '"key-press-event"',
    '"key-release-event"',
    '"leave-notify-event"',
    '"delete-event"',
    '"motion-notify-event"',
}
# Add old signal names with underscore.
signals_returning_values.update({sig.replace("-", "_") for sig in signals_returning_values})

def is_signal_returning_value(signal):
    return signal.strip() in signals_returning_values

@find_handlers@
identifier handler;
expression object, data;
expression signal : script:python() { not is_signal_returning_value(signal) };
typedef GCallback;
@@
/** TODO: Check that signal is defined with void return value. */
\( g_signal_connect \| g_signal_connect_after \| signal_connect_swapped \)(object, signal, \( G_CALLBACK (handler) \| (GCallback) handler \), data)


@voidize_defs depends on find_handlers@
type T;
identifier handler = find_handlers.handler;
@@
-T
+void
handler(...) { ... }


@voidize_declrs depends on find_handlers@
type T;
identifier handler = find_handlers.handler;
@@
-T
+void
handler(...);


@return_void depends on voidize_defs@
identifier handler = voidize_defs.handler;
constant exit_val;
// Lowercase true is matched as an identifier, not a constant.
identifier exit_val_id;
expression exit_val_preserved;
expression condition;
@@
void handler(...) {
<+...
(
// Variant 1: a constant like a number or bool is returned.
// Let’s remove it without hesitation.
-return \( exit_val \| exit_val_id \);
+return;
|
// Variant 2: a function is called in return statement.
// Let’s ensure it is run before returning.
-return exit_val_preserved;
+exit_val_preserved;
+return;
|
// Variant 3: a return macro is used.
// Let’s replace it with a one thar returns (void).
-g_return_val_if_fail(condition, \( exit_val \| exit_val_id \));
+g_return_if_fail(condition);
|
// Variant 4: a return macro is used.
// Let’s replace it with a one thar returns (void).
-g_return_val_if_reached(\( exit_val \| exit_val_id \));
+g_return_if_reached();
)
...+>
}
