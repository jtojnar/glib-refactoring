#!/usr/bin/env python3

import difflib
import json
import subprocess
import tempfile
import unittest
from pathlib import Path

patches_dir = Path.cwd() / "patches"
test_data_dir = Path.cwd() / "tests" / "data"


def assert_contents_equal(expected_file: Path, output_file: Path):
    expected_contents = expected_file.read_text().splitlines(keepends=True)
    output_contents = output_file.read_text().splitlines(keepends=True)
    diff = "".join(difflib.unified_diff(expected_contents, output_contents, fromfile=str(expected_file), tofile=str(output_file)))

    if diff:
        raise AssertionError(f"Output does not match the expected output:\n" + diff)


def make_test_example(patch, example):
    def case():
        patch_file = (patches_dir / patch).with_suffix(".cocci")
        input_file = (test_data_dir / example).with_suffix(".in.c")
        expected_file = (test_data_dir / example).with_suffix(".expected.c")

        with tempfile.TemporaryDirectory() as temp_directory:
            output_file = (Path(temp_directory) / example).with_suffix(".out.c")
            output_file.parent.mkdir(parents=True, exist_ok=True)

            test_build = subprocess.run(
                ["spatch", "--no-show-diff", "--sp-file", patch_file, input_file, "-o", output_file],
                check=True,
            )
            subprocess.run(
                ["clang-format", "--style={BasedOnStyle: llvm, IndentWidth: 4}", "-i", output_file],
                check=True,
            )

            assert_contents_equal(expected_file, output_file)

    return case


def make_test_rule(patch, examples):
    suite = unittest.TestSuite()
    for example in examples:
        suite.addTest(
            FunctionTestCase(
                make_test_example(patch, example),
                description=f"spatch({patch}.cocci) {example}.in.c",
            )
        )

    return suite


class FunctionTestCase(unittest.FunctionTestCase):
    # We do not care that the test class is unittest.case.FunctionTestCase (case)
    def __str__(self):
        return self._description

    # Do not print description twice
    def shortDescription(self):
        return None


class TestSuite(unittest.TestSuite):
    # do not run _removeTestAtIndex since there is nothing in _tests
    _cleanup = False

    def __iter__(self):
        yield make_test_rule(
            "do-not-cast-signal-connect-object",
            [
                "do-not-cast-signal-connect-object",
            ],
        )

        yield make_test_rule(
            "glib/autocleanup",
            [
                "glib/autocleanup",
            ],
        )

        yield make_test_rule(
            "glib/clear-object",
            [
                "glib/clear-object",
            ],
        )

        yield make_test_rule(
            "gtk2/direct-property-access-to-functions",
            [
                "gtk2/direct-property-access-to-functions",
            ],
        )

        yield make_test_rule(
            "gtk2/fixup-outparam-functions",
            [
                "gtk2/fixup-outparam-functions",
            ],
        )

        yield make_test_rule(
            "gtk2/keysym-compat",
            [
                "gtk2/keysym-compat",
            ],
        )

        yield make_test_rule(
            "gtk3/asynchronize-dialog-without-result",
            [
                "gtk3/asynchronize-dialog-without-result",
            ],
        )

        yield make_test_rule(
            "gtk3/cursor-deprecations",
            [
                "gtk3/cursor-deprecations",
            ],
        )

        yield make_test_rule(
            "voidize-signal-handlers",
            [
                "voidize-signal-handlers",
            ],
        )


def load_tests(loader, tests, pattern):
    return TestSuite()


if __name__ == "__main__":
    unittest.main(verbosity=2)
