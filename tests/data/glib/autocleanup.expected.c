void test() {
    g_autoptr(Foo) object_initialized_in_declaration = foo_new();

    g_autoptr(Bar) object_initialized_separately = NULL;
    object_initialized_separately = bar_new();

    g_autoptr(Baz) object_initialized_to_null_and_then_separately = NULL;
    object_initialized_to_null_and_then_separately = baz_new();
    if (random_failure()) {
        return;
    }

    g_autoptr(GList) list_unowned_elements = NULL;

    Qux *object_not_recognized = qux_new();
    qux_unref(object_not_recognized);

    g_autofree char *string_initialized_in_declaration = g_new0(char, 64);

    g_autofree char *string_initialized_separately = NULL;
    string_initialized_separately = g_new0(char, 64);

    g_autofree char *string_initialized_to_null_and_then_separately = NULL;
    string_initialized_to_null_and_then_separately = g_new0(char, 64);

    g_autoptr(GHashTable) table = g_hash_table_new();
}

static void inside_loop() {
    while (true) {
        g_autofree char *fullpath = NULL;
        g_autoptr(GFile) file = NULL;

        if (true) {
            g_autoptr(GFileInfo) info = NULL;
            if (info != NULL) {
            } else {
                if (!error_matches) {
                }
            }
        }
    }
}
