void test() {
    Foo *object_initialized_in_declaration = foo_new();
    g_object_unref(object_initialized_in_declaration);

    Bar *object_initialized_separately;
    object_initialized_separately = bar_new();
    g_object_unref(object_initialized_separately);

    Baz *object_initialized_to_null_and_then_separately = NULL;
    object_initialized_to_null_and_then_separately = baz_new();
    if (random_failure()) {
        g_object_unref(object_initialized_to_null_and_then_separately);
        return;
    }
    g_object_unref(object_initialized_to_null_and_then_separately);

    GList *list_unowned_elements = NULL;
    g_list_free(list_unowned_elements);

    Qux *object_not_recognized = qux_new();
    qux_unref(object_not_recognized);

    char *string_initialized_in_declaration = g_new0(char, 64);
    g_free(string_initialized_in_declaration);

    char *string_initialized_separately;
    string_initialized_separately = g_new0(char, 64);
    g_free(string_initialized_separately);

    char *string_initialized_to_null_and_then_separately = NULL;
    string_initialized_to_null_and_then_separately = g_new0(char, 64);
    g_free(string_initialized_to_null_and_then_separately);

    GHashTable *table = g_hash_table_new();
    g_hash_table_unref(table);
}

static void inside_loop() {
    while (true) {
        char *fullpath;
        GFile *file;
        g_object_unref(file);
        g_free(fullpath);

        if (true) {
            GFileInfo *info;
            if (info != NULL) {
                g_object_unref(info);
            } else {
                if (!error_matches) {
                    g_object_unref(info);
                }
            }
        }
    }
}
