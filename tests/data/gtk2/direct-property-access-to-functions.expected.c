static void on_toggled(GtkToggleButton *button, gconstpointer user_data) {
    GtkWidget *widget = GTK_WIDGET(button);
    GtkHBox *box;
    int w, h;
    gdk_drawable_get_size(gtk_widget_get_window(widget), &w, &h);

    gtk_toggle_button_set_draw_indicator(button, FALSE);

    if (gtk_toggle_button_get_active(button)) {
        frob_active();
    } else {
        frob_inactive();
    }

    box = GTK_H_BOX(gtk_widget_get_parent(widget));

    gtk_h_box_set_spacing(box, 5);

    printf("%d", gtk_toggle_button_get_inconsistent(button));
}

void foo() {
    Foo *widget = get_foo();
    // Do not change non-GTK classes
    widget->parent = NULL;
}

void unsealed(GtkAllocation *allocation) {
    // Do not change unsealed classes
    allocation->width = 5;
}

void bar(GtkButton *btn) {
    printf("%s", gtk_image_get_icon_name(GTK_IMAGE(gtk_button_get_image(btn))));
    gtk_image_set_icon_name(GTK_IMAGE(gtk_button_get_image(btn)),
                            "dialog-warning");
}

static void foo_widget_destroy(GtkWidget *widget) {
    GtkWidgetClass *parent_class = GTK_WIDGET_CLASS(foo_widget_parent_class);
    // Do not change class members
    parent_class->destroy(widget);
}
