static void on_toggled(GtkToggleButton *button, gconstpointer user_data) {
    GtkWidget *widget = GTK_WIDGET(button);
    GtkHBox *box;
    int w, h;
    gdk_drawable_get_size(widget->window, &w, &h);

    button->draw_indicator = FALSE;

    if (button->active) {
        frob_active();
    } else {
        frob_inactive();
    }

    box = GTK_H_BOX(widget->parent);

    box->spacing = 5;

    printf("%d", button->inconsistent);
}

void foo() {
    Foo *widget = get_foo();
    // Do not change non-GTK classes
    widget->parent = NULL;
}

void unsealed(GtkAllocation *allocation) {
    // Do not change unsealed classes
    allocation->width = 5;
}

void bar(GtkButton *btn) {
    printf("%s", (GTK_IMAGE(btn->image))->icon_name);
    GTK_IMAGE(btn->image)->icon_name = "dialog-warning";
}

static void foo_widget_destroy(GtkWidget *widget) {
    GtkWidgetClass *parent_class = GTK_WIDGET_CLASS(foo_widget_parent_class);
    // Do not change class members
    parent_class->destroy(widget);
}
