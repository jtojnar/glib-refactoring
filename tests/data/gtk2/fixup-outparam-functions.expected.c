void foo(GtkWidget *widget) {
    GdkRectangle rect;
    GdkAllocation *allocation_ptr;

    GtkAllocation allocation0;
    gtk_widget_get_allocation(widget, &allocation0);
    rect.width = allocation0.width;
    GtkAllocation allocation1;
    gtk_widget_get_allocation(widget, &allocation1);
    if (allocation1.height) {
        gtk_widget_set_allocation(widget, allocation_ptr);
    }
}

void bar(GtkWidget *widget) {
    GtkAllocation allocation2;
    gtk_widget_get_allocation(widget, &allocation2);
    GtkAllocation allocation3;
    gtk_widget_get_allocation(widget, &allocation3);
    GdkRectangle window_rect = {
        .x = 0,
        .y = 0,
        .width = allocation2.width,
        .height = allocation3.height,
    };
}

void methods_handling_multiple_params() {
    GtkCellRenderer *cr = gtk_cell_renderer_new();
    gtk_cell_renderer_set_alignment(cr, xalign, yalign);

    gint xpad5;
    gint ypad4;
    gtk_cell_renderer_get_padding(cr, &xpad5, &ypad4);
    gint xpad6;
    gint ypad7;
    gtk_cell_renderer_get_padding(cr, &xpad6, &ypad7);
    gint padding_multiplied = xpad5 * ypad7;
    gint width8;
    gint height9;
    gtk_cell_renderer_get_fixed_size(cr, &width8, &height9);
    gint width11;
    gint height10;
    gtk_cell_renderer_get_fixed_size(cr, &width11, &height10);
    gint area = height9 * width11;

    gtk_cell_renderer_set_padding(cr, 10, 10);

    g_object_set(cr, "mode", GTK_CELL_RENDERER_MODE_ACTIVATABLE, NULL);
}
