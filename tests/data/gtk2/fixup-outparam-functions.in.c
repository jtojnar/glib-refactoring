void foo(GtkWidget *widget) {
    GdkRectangle rect;
    GdkAllocation *allocation_ptr;

    rect.width = gtk_widget_get_allocation(widget).width;
    if (gtk_widget_get_allocation(widget).height) {
        gtk_widget_set_allocation(widget, *allocation_ptr);
    }
}

void bar(GtkWidget *widget) {
    GdkRectangle window_rect = {
        .x = 0,
        .y = 0,
        .width = gtk_widget_get_allocation(widget).width,
        .height = gtk_widget_get_allocation(widget).height,
    };
}

void methods_handling_multiple_params() {
    GtkCellRenderer *cr = gtk_cell_renderer_new();
    gtk_cell_renderer_set_xalign(cr, xalign);
    gtk_cell_renderer_set_yalign(cr, yalign);

    gint padding_multiplied =
        gtk_cell_renderer_get_xpad(cr) * gtk_cell_renderer_get_ypad(cr);
    gint area =
        gtk_cell_renderer_get_height(cr) * gtk_cell_renderer_get_width(cr);

    gtk_cell_renderer_set_xpad(cr, 10);
    gtk_cell_renderer_set_ypad(cr, 10);

    gtk_cell_renderer_set_mode(cr, GTK_CELL_RENDERER_MODE_ACTIVATABLE);
}
