#include <gdk/gdkkeysyms.h>

static gboolean should_mask(guint keyval) {
    if (keyval == GDK_KEY_Num_Lock) {
        return TRUE;
    } else {
        return keyval != GDK_KEY_P && keyval != GDK_KEY_Q;
    }
}
