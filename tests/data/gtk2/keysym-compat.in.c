#include <gdk/gdkkeysyms-compat.h>

static gboolean should_mask(guint keyval) {
    if (keyval == GDK_Num_Lock) {
        return TRUE;
    } else {
        return keyval != GDK_P && keyval != GDK_Q;
    }
}
