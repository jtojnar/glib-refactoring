static void check_if_world_is_on_fire_response_cb2(GtkDialog *dialog,
                                                   int response,
                                                   gconstpointer data) {
    foo1();
    gtk_widget_destroy(GTK_WIDGET(dialog));
    bar1();
}

static void check_if_world_is_on_fire_response_cb1(GtkDialog *dialog,
                                                   int response,
                                                   gconstpointer data) {
    foo();
    gtk_widget_destroy(GTK_WIDGET(dialog));
    bar();
}

void check_if_world_is_on_fire(GtkWindow *parent_window) {
    GtkWidget *dialog;
    g_autoptr(GFile) file =
        g_file_new_for_uri("https://api.is-the-world-on-fire.org");
    gsize length = 50;
    g_autofree char *contents = g_malloc(length);
    gboolean status;
    gboolean is_world_on_fire;

    status = g_file_load_contents(file, NULL, &contents, length, NULL, NULL);

    if (!status) {
        dialog = gtk_message_dialog_new(
            parent_window, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR,
            GTK_BUTTONS_CLOSE,
            _("Unable to determine whether the world is on fire."));

        g_signal_connect(dialog, "response", G_CALLBACK(gtk_widget_destroy),
                         NULL);
        gtk_widget_show(dialog);

        return;
    }

    if (!status) {
        dialog = gtk_message_dialog_new(parent_window, GTK_DIALOG_MODAL,
                                        GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE,
                                        _("Foo."));

        g_signal_connect(dialog, "response",
                         G_CALLBACK(check_if_world_is_on_fire_response_cb1),
                         NULL);
        gtk_widget_show(dialog);
        return;
    }

    if (!status) {
        dialog = gtk_message_dialog_new(parent_window, GTK_DIALOG_MODAL,
                                        GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE,
                                        _("Bar."));

        g_signal_connect(dialog, "response",
                         G_CALLBACK(check_if_world_is_on_fire_response_cb2),
                         NULL);
        gtk_widget_show(dialog);
        return;
    }

    is_world_on_fire = g_strcmp0(contents, "yes") == 0;

    dialog = gtk_message_dialog_new(
        parent_window, GTK_DIALOG_MODAL,
        is_world_on_fire ? GTK_MESSAGE_WARNING : GTK_MESSAGE_INFO,
        GTK_BUTTONS_CLOSE,
        is_world_on_fire ? _("This is not fine!!") : _("Everything is okay."));

    g_signal_connect(dialog, "response", G_CALLBACK(gtk_widget_destroy), NULL);
    gtk_widget_show(dialog);
}
