void cursor(GtkWidget *image) {
    GdkCursor *cursor;

    cursor = gdk_cursor_new_from_name(gtk_widget_get_display(GTK_WIDGET(image)),
                                      "crosshair");

    cursor = gdk_cursor_new_from_name(gdk_display_get_default(), GDK_HEART);

    cursor = gdk_cursor_new_from_name(gdk_display_get_default(), "pointer");
}
