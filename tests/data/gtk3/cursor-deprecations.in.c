void cursor(GtkWidget *image) {
    GdkCursor *cursor;

    cursor = gdk_cursor_new_for_display(
        gtk_widget_get_display(GTK_WIDGET(image)), GDK_CROSSHAIR);

    cursor = gdk_cursor_new(GDK_HEART);

    cursor = gdk_cursor_new(GDK_HAND2);
}
