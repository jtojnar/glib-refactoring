static void button_clicked_released_cb(GtkButton *button, char *data) {
    printf("Button was %s.\n", data);

    if (foo) {
        call_something_else();
        return;
    }

    if (bar) {
        return;
    }

    if (baz) {
        g_return_if_reached();
    }

    g_return_if_fail(uri != NULL);
    return;
}

static void button_something_cb(GtkButton *button, void *user_data) { return; }

static gboolean key_press_event_cb(GtkButton *button, void *user_data) {
    return TRUE;
}

void foo() {
    GtkWidget *button = gtk_button_new("Click me!");
    foo = g_signal_connect(button, "something", (GCallback)button_something_cb,
                           NULL);
    g_signal_connect(button, "clicked", (GCallback)button_clicked_released_cb,
                     "clicked");
    g_signal_connect(button, "released", (GCallback)button_clicked_released_cb,
                     "released");
    g_signal_connect(button, "key_press_event", (GCallback)key_press_event_cb,
                     NULL);
}
