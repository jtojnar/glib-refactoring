static gboolean button_clicked_released_cb(GtkButton *button, char *data) {
    printf("Button was %s.\n", data);

    if (foo) {
        return call_something_else();
    }

    if (bar) {
        return 42;
    }

    if (baz) {
        g_return_val_if_reached(NULL);
    }

    g_return_val_if_fail(uri != NULL, false);
    return true;
}

static gboolean button_something_cb(GtkButton *button, void *user_data) {
    return TRUE;
}

static gboolean key_press_event_cb(GtkButton *button, void *user_data) {
    return TRUE;
}

void foo() {
    GtkWidget *button = gtk_button_new("Click me!");
    foo = g_signal_connect(button, "something", (GCallback)button_something_cb,
                           NULL);
    g_signal_connect(button, "clicked", (GCallback)button_clicked_released_cb,
                     "clicked");
    g_signal_connect(button, "released", (GCallback)button_clicked_released_cb,
                     "released");
    g_signal_connect(button, "key_press_event", (GCallback)key_press_event_cb,
                     NULL);
}
